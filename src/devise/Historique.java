package devise;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.SessionScoped;


@SessionScoped
public class Historique implements Ihistorique, Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6954943993153345696L;
	private List<Retour> retour = new ArrayList<Retour>();
	public Historique() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void save(Retour retour) {
	this.retour.add(retour);
	
	}
	@Override
	public float save(float valeurInitiale, float valeurFinale ) {
		// TODO Auto-generated method stub
		
		Retour retour = new Retour();
		retour.setValeurFinale(valeurFinale);
		retour.setValeurInitiale(valeurInitiale);
		save(retour);
		
		return 0;
	}

	@Override
	public List<Retour> getRetours()
	{
	return retour;
	}

}
