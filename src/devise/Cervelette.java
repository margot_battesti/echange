package devise;

import java.io.IOException;

import javax.ejb.EJB;
import javax.inject.Inject;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Cervelette
 */
@WebServlet("/Cervelette")
public class Cervelette extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@EJB(beanName="euro")Iconvertisseur convertisseureuro;
	@EJB(beanName="dollar")Iconvertisseur convertisseurdollar;
	
	@Inject Ihistorique histo;
	//@Inject @Named("euro") Iconvertisseur convertisseureuro;
	//@Inject @Named("dollar") Iconvertisseur convertisseurdollar;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Cervelette() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nextJSP = "Convertisseur.jsp";
		request.setAttribute("histo",histo.getRetours());
		RequestDispatcher dispatcher = request.getRequestDispatcher(nextJSP);
		
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		
		String euro = request.getParameter("euro");
		Integer valeureuro=0;
		
		try {
			valeureuro = Integer.valueOf(euro);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
		float retoureuro = (float) convertisseureuro.convert(valeureuro);
		request.getSession().setAttribute("euro2", retoureuro);
		
	
		
		
		String dollar = request.getParameter("dollar");
		Integer valeurdollar=0;
		
		try {
			valeurdollar = Integer.valueOf(dollar);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
		float retourdollar = (float) convertisseurdollar.convert(valeurdollar);
		request.getSession().setAttribute("dollar2", retourdollar);
		request.getRequestDispatcher("Convertisseur.jsp").forward(request, response);
		
		
		request.setAttribute("histo",histo.getRetours());

		
	}

}
