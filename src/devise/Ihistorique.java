package devise;

import java.util.List;

public interface Ihistorique {

	public float save(float valeurInitiale, float valeurFinale );

	void save(Retour retour);

	List<Retour> getRetours();
	
}
